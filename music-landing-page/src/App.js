import React from 'react';
import './App.css';
import axios from 'axios';

class App extends React.Component {  
  constructor() {
    super();
    this.state = {
      assets: [],
      trackUrl: ''
    };
  }
  getAssets = () => {
    axios.get(`${process.env.REACT_APP_AGENTS_HOSTNAME}/static/info`)
      .then(assetResponse => { 
        this.setState({ 
          assets: assetResponse.data._embedded.Asset
        });
      });
  }

  getTrack = () => {
    axios.get(`${process.env.REACT_APP_AGENTS_HOSTNAME}/music/info`)
      .then(musicResponse => {
        this.setState({ 
          trackUrl: musicResponse.data.trackUrl
        });
      });
  }
  
  render() {
    return (
      <div className="App">
        <div>
          <button onClick={this.getTrack}>Get track</button>
          <button onClick={this.getAssets}>Get assets</button>
        </div>
        {this.state.trackUrl && 
          <audio src={this.state.trackUrl} controls autoPlay/>
        }
        <div>
          {this.state.assets && this.state.assets.map((asset, index) => {
            return <div key={index}>{asset.title}</div>
          })}
        </div>
        <div>
          {this.state.assets && this.state.assets.map((asset, index) => {
            return <img alt="oasis" key={index} src={asset.image} />
          })}
        </div>
      </div>
    );
  }
}

export default App;
